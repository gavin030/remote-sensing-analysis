"""
============================================================================
Remote Sensing Image Recognition Analysis based on KNN, RandomForest and SVM
============================================================================

Author: Haotian Shi, 2017

"""


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import time
import scipy.io
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import accuracy_score, roc_curve, auc

np.set_printoptions(threshold=np.inf)

# generate train and test data
mat = scipy.io.loadmat('data/sat-6-full.mat')
mat = {k:v for k, v in mat.items() if k[0] != '_'}

x_train = mat['train_x']
y_train = mat['train_y']
x_test = mat['test_x']
y_test = mat['test_y']

x_train = x_train.transpose((3, 0, 1, 2)).astype(np.float64) # covert n to be 1st axis
y_train = y_train.transpose((1, 0)).astype(np.int32) # y is already in binarized format
x_test = x_test.transpose((3, 0, 1, 2)).astype(np.float64)
y_test = y_test.transpose((1, 0)).astype(np.int32)

x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1] * x_train.shape[2] * x_train.shape[3])) # reshape x to be n * d format
x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1] * x_test.shape[2] * x_test.shape[3]))

x_train = x_train[:] # make training with partition convenient
x_test = x_test[:]
y_train = y_train[:]
y_test = y_test[:]

pca = PCA(n_components=5) # dimensionality reduction
pca.fit(x_train)
x_train = pca.transform(x_train)
x_test = pca.transform(x_test)

# train classifiers, record data
classes = {0:'barren land', 1:'trees', 2:'grass land', 3:'roads', 4:'buildings', 5:'water bodies'}

KNN_multiclass_clf = OneVsRestClassifier(KNeighborsClassifier(n_neighbors=8))
train_start_time = time.time()
KNN_multiclass_clf.fit(x_train, y_train)
print("KNN train time: ", time.time() - train_start_time)
predict_start_time = time.time()
y_test_prob_KNN = KNN_multiclass_clf.predict_proba(x_test)
print("KNN predict time: ", time.time() - predict_start_time)
fpr_KNN = dict()
tpr_KNN = dict()
roc_auc_KNN = dict()
for clazz in classes.keys():
  fpr_KNN[clazz], tpr_KNN[clazz], thre = roc_curve(y_test[:, clazz], y_test_prob_KNN[:, clazz])
  roc_auc_KNN[clazz] = auc(fpr_KNN[clazz], tpr_KNN[clazz])

RandomForest_multiclass_clf = OneVsRestClassifier(RandomForestClassifier(n_estimators=10, max_depth=5))
train_start_time = time.time()
RandomForest_multiclass_clf.fit(x_train, y_train)
print("RandomForest train time: ", time.time() - train_start_time)
predict_start_time = time.time()
y_test_prob_RandomForest = RandomForest_multiclass_clf.predict_proba(x_test)
print("RandomForest predict time: ", time.time() - predict_start_time)
fpr_RandomForest = dict()
tpr_RandomForest = dict()
roc_auc_RandomForest = dict()
for clazz in classes.keys():
  fpr_RandomForest[clazz], tpr_RandomForest[clazz], thre = roc_curve(y_test[:, clazz], y_test_prob_RandomForest[:, clazz])
  roc_auc_RandomForest[clazz] = auc(fpr_RandomForest[clazz], tpr_RandomForest[clazz])

SVM_multiclass_clf = OneVsRestClassifier(SVC(kernel='rbf', probability=True, C=2.2, gamma=1e-7))
train_start_time = time.time()
SVM_multiclass_clf.fit(x_train, y_train)
print("SVM train time: ", time.time() - train_start_time)
predict_start_time = time.time()
y_test_prob_SVM = SVM_multiclass_clf.predict_proba(x_test)
print("SVM predict time: ", time.time() - predict_start_time)
fpr_SVM = dict()
tpr_SVM = dict()
roc_auc_SVM = dict()
for clazz in classes.keys():
  fpr_SVM[clazz], tpr_SVM[clazz], thre = roc_curve(y_test[:, clazz], y_test_prob_SVM[:, clazz])
  roc_auc_SVM[clazz] = auc(fpr_SVM[clazz], tpr_SVM[clazz])

# generate ROC curves
colors = ['orange', 'red', 'darkviolet', 'gray', 'lime', 'dodgerblue']

plt.figure()
for clazz, color in zip(classes.keys(), colors):
  plt.plot(fpr_KNN[clazz], tpr_KNN[clazz], color=color, lw=1, label='{0}, KNN (auc = {1:0.2f})'.format(classes[clazz], roc_auc_KNN[clazz]))
plt.plot([0, 1], [0, 1], 'k--', lw=2)
plt.xlim([-0.05, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('KNN ROC curves for SAT-6 airborne datasets')
plt.legend(loc='lower right')
plt.savefig('KNNROC.png')

plt.figure()
for clazz, color in zip(classes.keys(), colors):
  plt.plot(fpr_RandomForest[clazz], tpr_RandomForest[clazz], color=color, lw=1, label='{0}, RandomForest (auc = {1:0.2f})'.format(classes[clazz], roc_auc_RandomForest[clazz]))
plt.plot([0, 1], [0, 1], 'k--', lw=2)
plt.xlim([-0.05, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('RandomForest ROC curves for SAT-6 airborne datasets')
plt.legend(loc='lower right')
plt.savefig('RandomForestROC.png')

plt.figure()
for clazz, color in zip(classes.keys(), colors):
  plt.plot(fpr_SVM[clazz], tpr_SVM[clazz], color=color, lw=1, label='{0}, SVM (auc = {1:0.2f})'.format(classes[clazz], roc_auc_SVM[clazz]))
plt.plot([0, 1], [0, 1], 'k--', lw=2)
plt.xlim([-0.05, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('SVM ROC curves for SAT-6 airborne datasets')
plt.legend(loc='lower right')
plt.savefig('SVMROC.png')
